package it.univet.visionar.auevisionarlauncher;

import android.content.Intent;
import android.os.Bundle;

import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.univet.visionar.libvarp.*;


public class MainActivity extends VisionARActivity {
    static private final String TAG = "AUE_LNCH";

    private List<ApplicationInfo> mPackages;
    private Handler mStatusHandler = new Handler();
    private int mPackagesIndex;
    private Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateBatteryInfo();
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mStatusHandler.postDelayed(mStatusChecker, 60000);
            }
        }
    };

    private ViewGroup mLayoutView;
    private TextView mTitle;
    private HorizontalScrollView mHsv;
    private LinearLayout mHsvLinearLayout;
    private SeekBar mBar;
    private ImageView mImgBattery;
    private boolean mConfirmWaiting;


    private class VisionARClientTaskBattery extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            final VisionARInputGetCUBatteryStatusResponseMessage response = new VisionARInputGetCUBatteryStatusResponseMessage(result);

            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                int lvlBattery = response.getExternalBatteryLevel();

                if (lvlBattery < 25){
                    VisionARLog.v(TAG, "Battery(" + lvlBattery + "): low battery.");
                    mImgBattery.setImageResource(R.drawable.low_battery);
                }
                else if (lvlBattery < 50 ){
                    VisionARLog.v(TAG, "Battery(" + lvlBattery + "): med low battery.");
                    mImgBattery.setImageResource(R.drawable.med_low_battery);
                }
                else if (lvlBattery < 75 ){
                    VisionARLog.v(TAG, "Battery(" + lvlBattery + "): med high battery.");
                    mImgBattery.setImageResource(R.drawable.med_high_battery);
                }
                else {
                    VisionARLog.v(TAG, "Battery(" + lvlBattery + "): high battery.");
                    mImgBattery.setImageResource(R.drawable.high_battery);
                }
            }

            mLayoutView.invalidate();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
        initView();

        vibration(100);
        try {
            Thread.sleep(200);
        } catch (Exception e) {
        }
        vibration(100);

        startRepeatingTask();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }


    @Override
    protected void onControlUnitKeyReceived(VisionARInputKeyNotification msg) {
        VisionARLog.i(TAG, "code = " + msg.getCode() + ", value = " + msg.getValue());

        boolean isInvalid = false;

        if (msg.getValue() == 1) {
            switch (msg.getCode()) {
                case VisionARMessage.DOWN_BUTTON_KEY:
                    if (mConfirmWaiting == true) {
                        ImageView icon = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_application_item_image);
                        TextView confirm = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_confirm_text);

                        icon.setVisibility(View.VISIBLE);
                        confirm.setVisibility(View.INVISIBLE);

                        mConfirmWaiting = false;

                        isInvalid = true;
                    }

                    if (mPackagesIndex > 0) {
                        mPackagesIndex--;
                        mHsv.scrollTo((mHsvLinearLayout.getChildAt(mPackagesIndex).getLeft()), mHsv.getScrollY());
                        mTitle.setText(mPackages.get(mPackagesIndex).packageName);
                        mBar.setProgress(mPackagesIndex);

                        isInvalid = true;
                    }
                    break;
                case VisionARMessage.ENTER_BUTTON_KEY:
                    if ( mPackages.size() > 0) {
                        ImageView icon = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_application_item_image);
                        TextView confirm = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_confirm_text);

                        if (mConfirmWaiting == false) {
                            // Open confirmation dialog box
                            icon.setVisibility(View.INVISIBLE);
                            confirm.setVisibility(View.VISIBLE);
                        }
                        else {
                            // Close confirmation dialog box
                            icon.setVisibility(View.VISIBLE);
                            confirm.setVisibility(View.INVISIBLE);

                            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(mPackages.get(mPackagesIndex).packageName);
                            if (launchIntent != null) {
                                startActivity(launchIntent);//null pointer check in case package name was not found
                                finishAffinity();
                                System.exit(0);
                            }
                        }

                        mConfirmWaiting = !mConfirmWaiting;

                        isInvalid = true;
                    }
                    break;
                case VisionARMessage.UP_BUTTON_KEY:
                    if (mConfirmWaiting == true) {
                        ImageView icon = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_application_item_image);
                        TextView confirm = mHsvLinearLayout.getChildAt(mPackagesIndex+1).findViewById(R.id.id_confirm_text);

                        icon.setVisibility(View.VISIBLE);
                        confirm.setVisibility(View.INVISIBLE);

                        mConfirmWaiting = false;

                        isInvalid = true;
                    }

                    if (mPackagesIndex < mPackages.size()-1) {
                        mPackagesIndex++;
                        mHsv.scrollTo((mHsvLinearLayout.getChildAt(mPackagesIndex).getLeft()), mHsv.getScrollY());
                        mTitle.setText(mPackages.get(mPackagesIndex).packageName);
                        mBar.setProgress(mPackagesIndex);

                        isInvalid = true;
                    }
                    break;

                default:
                    break;
            }

            if (isInvalid) {
                mLayoutView.invalidate();
            }
        } // if (msg.getValue() == 1)
    }


    private void initData() {
        final PackageManager pm = getPackageManager();

        //check apk
        /*
        File directory = new File(apkPath);
        File[] files = directory.listFiles();
        Log.d(TAG, "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {
            Log.d(TAG, "FileName:" + files[i].getName());
            if (files[i].getName().endsWith(".apk")) {
                Log.d(TAG, "Checking...");
                pm.getPackageInfo(files[i].getName());
            }
        }
         */

        //get a list of installed apps
        final List<ApplicationInfo> instPackages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        mConfirmWaiting = false;
        mPackages = new ArrayList<ApplicationInfo>();
        mPackagesIndex = 0;

        for (ApplicationInfo packageInfo : instPackages) {
            if (packageInfo.sourceDir.contains(getResources().getString(R.string.apps_path))) {
                // filtering custom apps...
                if (!packageInfo.packageName.equals(getPackageName())) {
                    // filtering this app...
                    VisionARLog.v(TAG, "name: " + packageInfo.packageName);
                    VisionARLog.v(TAG, "    activity: " + pm.getLaunchIntentForPackage(packageInfo.packageName));

                    mPackages.add(packageInfo);
                }
            }
        }
    }


    private void initView() {
        setContentView(R.layout.activity_main);

        mLayoutView = findViewById(R.id.view);

        mTitle = findViewById(R.id.title);
        mHsv = findViewById(R.id.hsv);
        mHsvLinearLayout = findViewById(R.id.hsvLinearLayout);
        mBar = findViewById(R.id.seekBar);
        mImgBattery = findViewById(R.id.battery);

        final LayoutInflater inflater = LayoutInflater.from(this);

        mHsvLinearLayout.removeAllViews();

        {
            FrameLayout imgLayout = (FrameLayout) inflater.inflate(R.layout.application_item, null);
            ImageView img = imgLayout.findViewById(R.id.id_application_item_image);
            img.setImageResource(0); // no image
            mHsvLinearLayout.addView(imgLayout, new ViewGroup.LayoutParams(140, 74));
        }

        if (mPackages.size() > 0) {
            for (ApplicationInfo packageInfo : mPackages) {
                FrameLayout imgLayout = (FrameLayout) inflater.inflate(R.layout.application_item, null);
                ImageView img = imgLayout.findViewById(R.id.id_application_item_image);
                if (packageInfo.icon != 0) {
                    img.setImageDrawable(packageInfo.loadIcon(getPackageManager()));
                }
                mHsvLinearLayout.addView(imgLayout, new ViewGroup.LayoutParams(140, 74));
            }

            mTitle.setText(mPackages.get(0).packageName);

            mBar.setMax(mPackages.size()-1);
        }
        else {
            FrameLayout imgLayout = (FrameLayout) inflater.inflate(R.layout.application_item, null);
            ImageView img = imgLayout.findViewById(R.id.id_application_item_image);
            img.setImageResource(0); // no image
            mHsvLinearLayout.addView(imgLayout, new ViewGroup.LayoutParams(140, 74));

            mBar.setMax(0);
        }

        {
            FrameLayout imgLayout = (FrameLayout) inflater.inflate(R.layout.application_item, null);
            ImageView img = imgLayout.findViewById(R.id.id_application_item_image);
            img.setImageResource(0); // no image
            mHsvLinearLayout.addView(imgLayout, new ViewGroup.LayoutParams(140, 74));
        }

        mHsv.scrollTo(0, mHsv.getScrollY());
        mBar.setProgress(0);

        mLayoutView.invalidate();
    }


    private void startRepeatingTask() {
        mStatusChecker.run();
    }


    private void stopRepeatingTask() {
        mStatusHandler.removeCallbacks(mStatusChecker);
    }


    private void vibration(int ms) {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName() + "(" + ms + ")");

        try {
            VisionAROutputSetHapticMessage message = new VisionAROutputSetHapticMessage(ms);
            VisionARClientTask task = new VisionARClientTask();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateBatteryInfo() {
        try {
            VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CU_BATT_STATUS);
            VisionARClientTaskBattery task = new VisionARClientTaskBattery();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
